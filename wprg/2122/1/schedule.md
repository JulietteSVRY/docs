# Календарный план "Веб-программирование"

### Занятия

| #   | Date  | Topic                                |
|-----|-------|--------------------------------------|
| 1   | 10.09 | JDBC Extra                           |
| 2   | 17.09 | Project Overview                     |
| 3   | 24.09 | HTTP. Servlets.                      |
| 4   | 01.10 | Project. Use Cases. Persistence. UI  |
| 5   | 08.10 | Spring Intro                         |
| 6   | 15.10 | Spring DI                            |
| 7   | 22.10 | Spring AOP. Project Lab              |
| 8   | 29.10 | Spring MVC                           |
| 9   | 05.11 | -                                    |
| 10  | 12.11 | JdbcTemplate. Hibernate. Spring Data |
| 11  | 19.11 | Spring Boot. Project Lab             |
| 12  | 26.11 | Project Lab                          |
| 13  | 03.12 | Spring Security                      |
| 14  | 10.12 | Project Lab                          |
| 15  | 17.12 | Project Lab                          | 
| 16  | 24.12 | Project Lab                          | 

### Web-приложение

| Этап проекта                                | Срок сдачи |
|---------------------------------------------|------------|
| Выбор темы                                  | 15.10      |
| Анализ задания и обзор аналогов             | 22.10      |
| Проектирование прецедентов использования    | 05.11      |
| Проектирование слоя персистентности         | 19.11      |
| Проектирование пользовательских интерфейсов | 03.12      |
| Сдача проекта и пояснительной записки       | 31.12      |

*Note: Рекомендуемый срок сдачи в среднем - хотя бы за неделю до указанного в таблице.

- Если не успеть в срок, применяется понижающий коэффициент **0,8**
- Если сильно не успеть в срок, применяется понижающий коэффициент **0,6**

### Autocode

| Блок упражнений | Мягкий Дедлайн | Дедлайн |
|-----------------|----------------|---------|
| JDBC. Extra     | 01.10          | 17.10   |
| Servlets        | 17.10          | 29.10   |
| Spring Core     | 07.11          | 19.11   |
| Spring MVC      | 19.11          | 03.12   |
| Spring Boot     | 10.12          | 24.12   |
| Spring Security | 24.12          | 31.12   |

- Если не успеть в **Мягкий Дедлайн**, применяется понижающий коэффициент **0,8**
- Если не успеть в **Дедлайн** , применяется понижающий коэффициент **0,6**

#### Упражнения

| Блок            | Упражнение                              | Баллы |
|-----------------|-----------------------------------------|-------|
| JDBC. Extra     | Employees. DAO.                         | 3     |
| JDBC. Extra     | Employees. Service.                     | 5     |
| Servlets        | Expression Calculator                   | 3     |
| Servlets        | Stateful Expression Calculator          | 5     |
| Spring Core     | Spring Chess Puzzles                    | 2     |
| Spring Core     | Spring Video Beans                      | 2     |
| Spring Core     | Spring Custom Scopes                    | 4     |
| Spring MVC      | Stateful Expression Calculator (Spring) | 4     |
| Spring Boot     | Employee Catalog                        | 8     |
| Spring Security | Spring Catalog Access                   | 4     |