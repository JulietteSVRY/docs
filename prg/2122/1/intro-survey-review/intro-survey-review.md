# Обзор результатов предварительного опроса.
## Программирование 2021 Осень. Первый семестр.

Здесь собрана сводка результатов опроса.
Где применимо, даны диаграммы. 
Ответы анонимизированы.
Комментарии студентов отобраны: некоторые из них убирал, чтобы было информативнее и показательнее.

Всего опрос прошло 49 человек.

### Поступление
![img.png](img.png)

Похоже, в целом поступление прошло достаточно гладко, хоть и не без отдельных проблем.

Эх, в былые-то времена какие очереди да толпы бывали, да никакой электронщины - все ногами да сканы-копии носишь.
Иногда прямо были игры престолов с выбором специальностей да распеределением поступающих.
Теперь-то уж не то, конечно, но и к лучшему. Хорошо, что процедуру лучше отладили.

#### Комментарии
>лучшая подача документов среди 5 других вузов

>Постепенно со всем разобралась, но были большие проблемы с выбором расписания, сайт упал, потом решили проблему, но нервы уже были ни к черту

>Слишком долго, а так всё легко

>Не понравилась практика одной волны из-за полной неразберихи

>Было очень увлекательно, много интерактивная, интересные станции, и в целом получил отличные эмоции

>Если имеется ввиду именно процедура подачи доков и тд, то это было в несколько раз лучше, чем в других вузах, единственное только то, что обратная связь была долговатая, но это уже издержки.

>Оперативно и на простом языке.

>Возникали проблемы в работе портала поступления. Сами сотрудники не знали правила поступления. Не начислили баллы за олимпиаду, которую сдавал в самом ИТМО.
                           
### Программирование

![img_1.png](img_1.png)

Здорово, что многие в программировании находят радость творчества.
Когда-то и меня в программирование привела эта дорога приключений.
Но и остальные варианты хороши, это оказался один из самых интересных вопросов.

#### Ответы
>нравится, когда кто-то делает то, что ты сказал ему делать

>Я считаю это очень интересным, но пока мой опыт заключается лишь в подготовке к егэ...

>У него много применений, но почему нравится - непонятно. Просто

>Заставляет подумать, и мне нравится решать интересные задачки

>Даже не знаю, почему именно. В жизни люблю прежде всего выстраивать порядок действий для какого-либо дела. Ну и еще перфекционист немного, нравится, когда все по своим местам лежит. Плюс ко всему курсы по программированию (по крайней мере по Python, не знаю, как с другими языками будет обстоять вопрос) давались довольно легко, во время решений задач прям азарт какой-то берет.

>Развивает мозг)

>Программирование дает мне суперсилу

>Бывают интересные задачи, с каждым разом узнаешь новое и можешь реализовать свои идеи все лучше и лучше

>Это захватывающие чувство, когда ты творишь

>Нравится, душа лежит создать что-то своими руками, но не всегда приходит правильная идея (говорю по опыту по подготовке к сдачи ЕГЭ)

>Программирование ускоряет жизнь

>потому что в современном мире нас так часто окружают компьютеры, что программирование зачастую можно прировнять с познанием мира

>Все мы знаем как приятно, когда твой код работает как надо

>Решение сложных и интересных задачек приносит мне удовольствие, и программирование помогает мне достичь моих целей

>А кому не нравится программировать :), программирование это преодоление себя, когда ты 2-3 часа сидишь и думаешь как придумать логику для решения задачи, а потом объяснять её компьютеру

>Я люблю программировать, поэтому учусь в ИТМО.

>Потому что это единственное, что у меня получается, кроме музыки, а музыка - самая бесперспективная профессия

>Потому что именно в программировании больше всего требуется логическое мышление

>Мне нравится решать задачи подобного типа, думать о том, какой должен быть алгоритм действий для выполнения задачи

>Мое увлечение математическими моделями находит свое применение

>потому что программирование выглядит круто

>Программирование Только две вещи я ненавижу - переводить сложные математические формулы в код и сложные API.

### Любимый язык
![img_2.png](img_2.png)

Чтобы было понятнее, лидеры - Python и C++.
Все остальное значительно меньше набрало голосов.

Интересны варианты мотивации. 
В основном, конечно, просто это ваш первый язык и его вы использовали для подготовки к ЕГЭ.

#### Комментарии

>(Python) Предпочтения пока не сложились, выбрал питон, потому что хорошо его знаю

>(Python) Показался мне проще и интересней других

>(Python) Легкий в изучении(наверное, далеко не только я так считаю, правда?), для егэ-шных задач по информатике отлично мне подошел.

>(C++) Я изучал несколько языков (С++, Delphi, Pascal, Python), Python мне не очень нравится, т.к он нетипизированный(следовательно проигрывает очень много времени при выполнении программы, проверял сам), Delphi - паскале видный язык, на втором для меня месте, т. к он написан на самом себе(большая часть классов написаны на Delphi), но он перешёл под управление embercadero. А плюсы очень похожи на делфи по структуре программы + он современный и всегда обновляется

>(C++) C++ быстро работает

>(C++) Этот язык очень помог мне лучше понять программирование, в том числе вещи, которые есть в других языках. Также C++ используется в той области, которая мне интересна(программирование графики).

>(JS) Я уже знаю несколько языков программирования, но мне очень нравится JavaScript, потому что он связан с созданием моего сайта.

>(C#) На C# я написал немало программ которые помогли мне а также игры на движке Unity

### Инструменты и сервисы
![img_3.png](img_3.png)

В основном вы говорили, что если изнакомы с чем-то, то это git, реже - Stepik.
Со всем остальным тоже кто-то нет-нет, да и знаком, но таких мало.
Разумеется, многие писали, что это все для них в новинку. 
Теперь-то уж вы, наверное, получше понимаете, проблема это или нет.

Особенно мне понравился последний из приведенных ответов. Преисполненный.

#### Ответы
>Почти всё как-то знакомо (кроме Stepik), но некоторыми вещами я пользуюсь редко (git, Gitlab, gcc) либо пользовался давно (IntelliJ, Maven). CLion не пользовался.

>Умею пользоваться и, соответственно, пользовался git, github, gcc, stepik. С остальным не знаком

>Пару раз пользовался git, прохожу курсы по программированию на Stepik, слышал про Maven, но не пользовался, IntelliJ IDEA

>Практически ничего

>Мне знакомо всё

### Дистант и очный режим
![img_4.png](img_4.png)

![img_5.png](img_5.png)

В целом, похоже, что почти все были готовы ко всему, но не стоит забывать и про тех, кто на опросник не отвечал.

По комментариям видно, что многие люди устали от дистанционки в школе, хотят очного режима.
У многих дистант в школе оставил негативные впечателения, и поэтому были опасения, как оно будет здесь.
Наверное, теперь вы уже можете сказать, реализовались они или нет.
Мне иногда тоже хочется пары очно вести, но все пять групп ни одна аудитория у нас не вмещает, а удваивать занятия я уже не могу.
Да и бонус в виде записей стримов тоже тогда пропадают. 
И только что прошла у нас полностью дистантная неделя. Так что, наверное, сейчас вы могли бы более взвешенно проголосовать.

#### Комментарии
>Хочу очно

>Ехать к первой паре 2 часа конечно не очень, но было бы интересно если бы чередовали недели

>Мне гораздо удобнее учиться дистанционно, но главные экзамены (для которых нужен проктор, если сдавать дистанционно) предпочитаю очно, так как не знаю, в чью пользу будут решаться технические проблемы.

>Лучше очно, если честно, наверно, уже все устали от вечной дистанционнки за эти два года, иногда просто хочется увидеть людей, пообщаться , спросить у учителя в живую

>Интернет хороший, компьютер вроде тоже, да и добраться проблем не доставит(я живу прям рядом со станцией метро)

### Планы на дисциплину
![img_6.png](img_6.png)

В целом, план дисциплины вас устроил. 

### Ожидания
Здесь ответы очень субъективные. Я почти не отбирал ответы, чтобы вы могли ощутить весь спектр ощущений. 

![img_7.png](img_7.png)

#### Ответы
>Страшно, что окажусь не очень сообразительной или долго придется понимать некоторые вещи, плюс, страшно за отсутствие времени

>Настроение впорядке

>Опасаюсь только того, что что-то не пойму, буду усердно трудиться

>Замотивировано

>Всё подругому, немного расстерен

>Ожидаю кучу нового материала, опасаюсь не перейти на следующий семестр

>Ощущение пока 0, честно говоря

>опасаюсь, что заказ еды в столовой через приложение не сделают

>Опасаюсь нехватки первоначальных знаний

>Я ожидаю получить много знаний, ожидаю прекрасную программу обучения

>Уверенно

>Ожидаю много новой информации :)

>Ожидаю много трудностей,но буду стараться их преодолевать.Если все будет хорошо,то я ожидаю ,что стану отличным программистом и смогу найти свое место в этом мире

>Ожидаю интересных лекций по всем предметам, но опасаюсь сложной практики по математике.

>Опасаюсь, что дистанционное обучение будет доступно не для всех предметов.

>Ожидаю, что будет сложно, так что надо будет перечитывать лекции

>Надеюсь смогу приспособиться

>Оч страшно, что преподы будут душить, но я думаю, что все мы люди, все всё поймут чуть что. А так поступил куда хотел, пока по кайфу)

>Боюсь, что будут проблемы в понимании, и что не буду все успевать

>Жду великого

>Опасаюсь возвращения к полностью дистанционному формату

>Люблю жабу, боюсь жабу…

>Я плохо изучаю русский язык, боюсь, что не пойму требований теста.

>присутствует только страх, ведь на всех встречах говорят о большом количестве отчисленных

>Настороженно; предстоит много нового узнать, а главное усвоить; лишь бы Game over не наступило раньше времени

>Ожидаю большой поток знаний и опасаюсь не успеть всё сдавать, запоминать.

>Пока не знаю чего ожидать, немного волнительно конечно, но справимся

>Опасаюсь того, что надоест


### Что будет иначе
![img_8.png](img_8.png)

Люди ждут самостоятельности, специализации, других процессов.
Подумайте, оправдались эти ожидания или нет.

#### Ответы
>Полное распределение своего времени самостоятельно

>Вся ответственность лежит на тебе самом, никто не будет тебя заставлять и бегать ща тобой. Я только "за", главное - не пропустить ничего и поспевать за всеми

>Никто не указывает, что делать

>Большая творческая свобода и огромное количество самостоятельной работы

>Всё, что мы делаем - делаем только для себя

>Опора исключительно на собственную инициативу

>Нет много общеобразовательного, благо чему можно сфокусироваться на том, что тебе интересно

>Более направленное обучение, в два раза более длинные уроки без увеличения перерывов, больший размер группы, более долгий путь от дома к образовательному учреждению.

>Познавательное дистанционное обучение

>Скорость преподавания, на матеше я уже прочувствовал эту боль. А так, наверное, в вуз мне хотя бы будет интересно ходить, а то школа совсем уж душное заведение

>У меня больше друзей и я буду учиться более серьезно

>Так как школа была максимально приближена к вузу, то только уменьшение количества учебного времени

>столовая теперь классная

### Учебный план

![img_9.png](img_9.png)

Не все смотрели учебный план, и в основном он всех устраивает.
Но кое-что некоторые хотели бы изменить.
Интересно, как бы вы сейчас на этот вопрос ответили.

#### Ответы
>Надо отучиться, поработать по специальности, а потом я смогу сказать что было бы нужно, а что нет

>можно было бы ещё пару проги и какой-нибудь математики.

>Моё направление - "Нейротехнологии и программирование". Убрал бы общую историю, добавил бы историю вычислительной техники, языков программирования, искусственного интеллекта и нейроинтерфейсов. Сделал бы упор на язык программирования, часто использующийся при разработке ИИ - Python. Половину занятий английского посвятил бы изучению англоязычной литературы на тему науки и программирования.

>Хочу C# :(

>Добавила бы больше пар по программированию

>Хотелось бы, может быть поверхностно, но коснуться темы ООП в С++

>Физику, хочу убрать физику, со всей силы хочу!!!

>Добавила бы на первом семестре курсы по эмоциональному интеллекту, ведь всем так сложно привыкнуть к новому темпу жизни

### Дополнения к программированию
![img_10.png](img_10.png)

Та же история - в целом устраивает, пожеланий немного.
И снова интересно, что бы вы ответили теперь.

#### Ответы

>Ожидаю объяснения основ

>Жду много интересного из мира программистов

>Я бы хотела создавать проекты, программы, писать ботов

>Жду новых знаний и испытаний. Хочу больше плюсов( но этому не быть).

>Хотелось бы больше C++, особенно сложных тем

>Просто хочу востребованную специальность

>Я жду изучения новых языков, сред программирования и т.д. Для меня в принципе все новым будет. В любом случае, хочется побольше углубиться в программистскую "движуху".

>Язык программирования, часто используемый для разработки искусственного интеллекта - Python. Использование ИИ-инструментов при написании кода и для поиска ошибок.

>Хотелось бы начать изучать Spring, узнать solid, dry, разобрать некоторые вопросы, задаваемые на собеседованиях на junior Java-разработчика

>Swift

### Пожелания
![img_11.png](img_11.png)

По пожеланиям, разумеется, виден и пережитый опыт.
Как бы вы сейчас их дополнили? 

#### Ответы

>Делать все в срок, тогда не будет проблем

>Учитесь понимать материал, а не зазубривать его так, как написано в книге. Прогайте побольше в свободное время!!

>пишите олимпиады и не волнуйтесь за егэ

>Прокачивайте самодисциплину

>Пожалуйста, не будьте такими пассивными, в первый день в универе наша группа была максимально молчаливой, никто друг с другом не разговаривал, хотя казалось бы, что еще столько учиться вместе. Так что пришлось знакомиться с несколькими людьми самому, чтобы хоть какая-то компания начала образовываться, а то ходить одному по новому для тебя месту с самого начала года - испытание то еще, особенно если ты интроверт.

>Подавать документы заранее, а не в последний момент

>Готовьтесь не к егэ, а олимпиадам

>Меньше переживать, егэ это просто шаблон

>Мы поступили, и вы поступите!

>Я бы сказал в какой вагон садиться чтобы ближе к выходу оказываться в метро, а так ничего

>Здоровья

### Заключение
Вас еще будут ждать опросы после первого семестра, и кое-какие результаты можно будет и сравнить.
Но уже сейчас вы можете сравнить свои ожидания и реальность в середине первого семестра.
Я думаю, это даст пищу для размышлений.
Будет здорово, если вы напишите, что именно в результатах вам показалось интересным или необычным. Или просто у вас есть к чему-то комментарий.
Ну и заодно можете сообщить, полезен ли был опрос для вас (мне-то уж точно), и стоит ли их делать дальше.
Может быть, сможете предложить какие-то еще вопросы, что стоило бы добавить или изменить, удалить.

