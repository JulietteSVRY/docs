# Список источников

*Note:* Список будет пополняться.

## Практика

- **Курс в Autocode**:\
[https://autocode.lab.epam.com/student/group/235](https://autocode.lab.epam.com/student/group/235)

## Рекомендуемая литература
- В. Спрингер - **Гид по Computer Science**\
Тонкая универсально полезная книжка. Рассказано об оценке сложности алгоритмов, самих алгоритмов и структур данных, а также приемах их проектирования.
- К. Хорстманн - **Java. Библиотека профессионала.** 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java.
Очень большой, подробный и мой любимый.
Описано почти все, что вам может понадобиться из Java SE.
- И.Н. Блинов, В.С. Романчик - **Java from EPAM**\
Не очень толстая, но емкая книга, охватывающая основные топики Java SE и начала Enterprise.\
Важно, что в отличие от большинства книг, ее можно легально и бесплатно в электронном формате получить тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books)\
К слову, там есть и другие хорошие легальные бесплантые книги - по тестированию, БД и прочее.

## Дополнительные материалы

### git
- S. Chacon, B. Straub. **Pro Git.**\
    Основная книга по Git. Распространяется бесплатно в электронном виде. Купить напечатанную версию тоже, конечно, можно. 
    - Оригинал: [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)
    - Перевод: [https://git-scm.com/book/ru/v2](https://git-scm.com/book/ru/v2)
- **Онлайн курс по Git от EPAM**: [https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e)
- **Онлайн курс по Git от devcolibri**: [https://devcolibri.com/course/git-для-начинающих/](https://devcolibri.com/course/git-%D0%B4%D0%BB%D1%8F-%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%89%D0%B8%D1%85/)
- **Вводная статья по Git от JRebel + CheatSheet**: [https://www.jrebel.com/blog/git-cheat-sheet](https://www.jrebel.com/blog/git-cheat-sheet)
- **Git CheetSheet от GitHub**:[https://training.github.com/downloads/ru/github-git-cheat-sheet/](https://training.github.com/downloads/ru/github-git-cheat-sheet/)
- **Интерактивный Git CheetSheet**: [https://ndpsoftware.com/git-cheatsheet.html](https://ndpsoftware.com/git-cheatsheet.html)
- Перевод старой известной статьи **A successful Git branching model**, описывающей Git Flow: [https://habr.com/ru/post/106912/](https://habr.com/ru/post/106912/)
- **Learning Git Branching**: [https://learngitbranching.js.org/](https://learngitbranching.js.org/) \
    Интерактивный тренажер по освоению ветвления в git

### Collections. Generics
- **Итератор как паттерн**. Refactoring.guru: https://refactoring.guru/ru/design-patterns/iterator
- Справочник по **Java Collections Framework**. Хабр: https://habr.com/ru/post/237043/
- Пришел, увидел, обобщил: погружаемся в **Java Generics**. Хабр: https://habr.com/ru/company/sberbank/blog/416413/

### Maven
- Apache Maven Project. **Maven in 5 minutes**: [https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)\
    Микрогайд от разработчиков
- Apache **Maven Tutorial. Baeldung**: [https://www.baeldung.com/maven](https://www.baeldung.com/maven)\
    Микрогайд от мастеров микрогайдов
- **Maven Tutorial. Jenkov**: [http://tutorials.jenkov.com/maven/maven-tutorial.html](http://tutorials.jenkov.com/maven/maven-tutorial.html)\
    Гайд от серьезного человека, с подробностями
- Chad Darby, **Maven tutorial** Youtube Playlist: [https://www.youtube.com/watch?v=Fe6lrsPmseo&list=PLEAQNNR8IlB7uvr8EJbCNJq2I82A8cqE7](https://www.youtube.com/watch?v=Fe6lrsPmseo&list=PLEAQNNR8IlB7uvr8EJbCNJq2I82A8cqE7)\
    Видеогайд от улыбчивого человека (подробностей поменьше)

### JUnit
- **JUnit 4 Wiki**: [https://github.com/junit-team/junit4/wiki](https://github.com/junit-team/junit4/wiki)
- **JUnit 4 Parameterized**: [https://www.tutorialspoint.com/junit/junit_parameterized_test.htm](https://www.tutorialspoint.com/junit/junit_parameterized_test.htm)
- **JUnit 5 User Guide**: [https://junit.org/junit5/docs/current/user-guide/](https://junit.org/junit5/docs/current/user-guide/)
- **JUnit 5 Baeldung Guide**: [https://www.baeldung.com/junit-5](https://www.baeldung.com/junit-5)
- **JUnit 5 Parameterized Tests**: [https://www.baeldung.com/parameterized-tests-junit-5](https://www.baeldung.com/parameterized-tests-junit-5)

### Lambdas. Streams.
- **Курс по лямбдам и стримам в Java.**\
    Ни секунды видео, один лишь текст и примеры. Задачки тоже есть.\
    [https://stepik.org/course/1595](https://stepik.org/course/1595)
- **Подробное руководство по стримам на русском с хорошей визуализацией**.\
    [https://annimon.com/article/2778](https://annimon.com/article/2778)
- **О стримах от их разработчика**
    - Часть 1. https://www.youtube.com/watch?v=O8oN4KSZEXE
    - Часть 2. https://www.youtube.com/watch?v=i0Jr2l3jrDA
- **О задачах и стримах от Тагира**. Он разбирается.\
    [https://www.youtube.com/watch?v=vxikpWnnnCU](https://www.youtube.com/watch?v=vxikpWnnnCU)

### I/O
- **Java IO Tutorial** (Jenkov): [http://tutorials.jenkov.com/java-io/index.html](http://tutorials.jenkov.com/java-io/index.html)
- **Java NIO Tutorial** (Jenkov, veeeery optional): [http://tutorials.jenkov.com/java-nio/index.html](http://tutorials.jenkov.com/java-nio/index.html)
- **Java NIO.2 Introduction** (Baeldung): [https://www.baeldung.com/java-nio-2-file-api](https://www.baeldung.com/java-nio-2-file-api)


### Concurrency
- **Java Concurrency Tutorial** (Jenkov): [http://tutorials.jenkov.com/java-concurrency/index.html](http://tutorials.jenkov.com/java-concurrency/index.html)
- **Справочник по синхронизаторам java.util.concurrent.\*** (Habr): [https://habr.com/ru/post/277669/](https://habr.com/ru/post/277669/)
- Brian Goetz. **Java Concurrency in practice**\
Классика и мастрид для професисональных Java-программистов. Здесь рассказывают, почему: [https://habr.com/ru/company/piter/blog/451322/](https://habr.com/ru/company/piter/blog/451322/)
- **Concurrent Collections**: 
[https://www.logicbig.com/tutorials/core-java-tutorial/java-collections/concurrent-collection-cheatsheet.html](https://www.logicbig.com/tutorials/core-java-tutorial/java-collections/concurrent-collection-cheatsheet.html)\
Сводка основных потокобезопасных коллекций - как они работают и зачем нужны.
- **Concurrent Maps**: 
[https://www.logicbig.com/tutorials/core-java-tutorial/java-collections/concurrent-map-cheatsheet.html](https://www.logicbig.com/tutorials/core-java-tutorial/java-collections/concurrent-map-cheatsheet.html)\
Сводка основных потокобезопасных мап (Map) - как они работают и зачем нужны.
- И.Н. Блинов, В.С. Романчик - **Java from EPAM**\
Дублирую ссылку на книгу специально для того, чтобы отметить, что в ней есть подробная глава о многопоточности. Саму книгу можно бесплатно и легально забрать тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books)
- Сергей Куксенко - Как сделать **CompletableFuture еще быстрее**:
[https://www.youtube.com/watch?v=W7iK74YA5NM](https://www.youtube.com/watch?v=W7iK74YA5NM)\
Доклад не для начинающих, но поможет разобраться с эффективностью CompletableFuture получше, если тема многопоточности вам не лишняя.

### SQL+JDBC
- **DB for FT Lab**:
[https://learn.epam.com/detailsPage?id=6449a5b9-d556-474a-8df5-fd96fcc2f4d5](https://learn.epam.com/detailsPage?id=6449a5b9-d556-474a-8df5-fd96fcc2f4d5)\
Небольшой бесплатный курс о БД - как они устроены, орагнизованы, как из проектировать и как работать с SQL.
- **Java. JDBC**:
[https://learn.epam.com/detailsPage?id=6fbe38da-0537-403e-9b04-4b4c8d3c52f6](https://learn.epam.com/detailsPage?id=6fbe38da-0537-403e-9b04-4b4c8d3c52f6)\
Небольшой курс о JDBC.
- **Книги** от авторов из EPAM, можно бесплатно и легально забрать тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books):
    - С.C. Куликов - **Реляционные Базы Данных в Примерах** 
    - С.C. Куликов - **Работа с MySQL, MS SQL SERVER и Oracle в примерах**
    - И.Н. Блинов, В.С. Романчик - **Java from EPAM** (Здесь есть глава о JDBC)
- **SQL Bolt**: [https://sqlbolt.com](https://sqlbolt.com)\
Прекрасный тренажер для работы с SQL, прекрасно подходит для новичков, чтобы разобраться с SQL.
Если есть проблемы с первым заданием из блока SQL&JDBC, попробуйте Sql Bolt.

### Swing
- Иван Портянкин. **Java Swing: Эффектные пользовательские интерфейсы**: [https://ipsoftware.ru/books/swing_book_2/](https://ipsoftware.ru/books/swing_book_2/)\
Отличная книга по Swing. Легально бесплатна в электронном виде.
- **Swing Tutorial** от JavaTpoint [https://www.javatpoint.com/java-swing](https://www.javatpoint.com/java-swing)\
Хороший обзор основных элементов Swing с иллюстрациями.
- **Swing Tutorial** от Oracle [https://docs.oracle.com/javase/tutorial/uiswing/TOC.html](https://docs.oracle.com/javase/tutorial/uiswing/TOC.html)\
Более объемный туториал, официальный.

### Java FX
- **Учебник по Java FX** в формате набора статей:
    - оригинал: [https://www.vojtechruzicka.com/javafx-getting-started/](https://www.vojtechruzicka.com/javafx-getting-started/)
    - перевод на Хабре: [https://habr.com/ru/post/474292/](https://habr.com/ru/post/474292/)
- **Java FX Tutorial** от **Jenkov**: [http://tutorials.jenkov.com/javafx/index.html](http://tutorials.jenkov.com/javafx/index.html)\
Подробный туториал, хороший обзор множества элементов
- **Java FX Bonus Chapter**: [https://horstmann.com/corejava/corejava_11ed-bonuschapter13-javafx.pdf](https://horstmann.com/corejava/corejava_11ed-bonuschapter13-javafx.pdf)\
Бонусная бесплатная глава для двухтомника Хорстмана про JavaFX.

### Telegram Bots
- **Как создать своего бота в BotFather?**: [https://botcreators.ru/blog/kak-sozdat-svoego-bota-v-botfather/](https://botcreators.ru/blog/kak-sozdat-svoego-bota-v-botfather/)
- **Telegram-бот на Java для самых маленьких**: [https://habr.com/ru/post/528694/](https://habr.com/ru/post/528694/)
- **Bots: Introduction for Developers**: [https://core.telegram.org/bots](https://core.telegram.org/bots)\
Зачем нужны и что умеют боты.
- **TG Bots API Java**: [https://core.telegram.org/bots/samples#java](https://core.telegram.org/bots/samples#java)
